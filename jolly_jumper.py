#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import sys

def jollyjumper():
    secuencia = input('Introduce la secuencia: ')
    separador = " "
    separado_por_espacios = secuencia.split(separador)
    longitud_secuencia = len(secuencia)
    primer_numero = ((int(secuencia) // 10 ** (longitud_secuencia - 1)) % 10)
    lista = []
    contador = 0
    for i in range(1, longitud_secuencia + 1):
        n = ((int(secuencia) // 10 ** (longitud_secuencia - i)) % 10)
        lista.append(n)
    for x in range(1, longitud_secuencia):
        primer_operador = lista[x - 1]
        segundo_operador = lista[x]
        resultado = primer_operador - segundo_operador
        if (resultado >= 0):
            resultado = resultado
        else:
            resultado = -resultado

        if (resultado >= 1) and (resultado <= primer_numero - 1):
            contador = contador + 1

    if (contador == longitud_secuencia - 1):
        print('Jolly')
    else:
        print('Not jolly')


jollyjumper()
